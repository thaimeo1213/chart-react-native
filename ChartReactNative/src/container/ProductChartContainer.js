import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { connect } from "react-redux";
import {
    getReportsProducts
} from '../store/redux/chart/actions'
import moment from 'moment'
class ProductChartContainer extends Component {
    componentDidMount() {
        const { getReportsProducts } = this.props
        const params = {
            '_locale': 'user',
            'after': moment().startOf('month').format('YYYY-MM-DDTHH:mm:ss'),
            'before': moment().endOf('month').format('YYYY-MM-DDTHH:mm:ss'),
            'extended_info': 'true',
            'order': 'desc',
            'orderby': 'items_sold',
            'page': 1,
            'per_page': 25
        }
        getReportsProducts(params)
    }

    render() {
        return (
            <View>
                <Text>Check</Text>
            </View>
        );
    }
}

const mapStateToProps = ({ chartReducer }) => {
    const {
        repost_products,
        loading,
        error
    } = chartReducer;

    return {
        repost_products,
        loading,
        error
    };
};

export default connect(
    mapStateToProps,
    {
        getReportsProducts,
    }
)(ProductChartContainer);

