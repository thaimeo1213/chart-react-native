import WooCommerceAPI from "./WooCommerceAPI";

export default class WooWorker {
  _api = null;

  static init = ({
    url,
    consumerKey,
    consumerSecret,
    wpAPI = true,
    version = "wc/v2",
    queryStringAuth = true,
    language
  }) => {
    try {
      this._api = new WooCommerceAPI({
        url,
        consumerKey,
        consumerSecret,
        wpAPI,
        version,
        queryStringAuth,
        language
      });
    } catch (error) {
      console.log(error);
    }
  };
  static get = async (url, params) => {
    try {
      const response = await this._api.get(url, params).then(response => {
        return response;
      })
      return response.json();
    } catch (err) {
      console.log(err)
    }
  }
}