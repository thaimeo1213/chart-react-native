/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import { Provider } from 'react-redux';
import { configureStore } from "./src/store/redux/store";
import Route from './src/Route'

const App: () => React$Node = () => {
  return (
    <Provider store={configureStore()}>
      <Route></Route>
    </Provider>

  );
};
export default App;
